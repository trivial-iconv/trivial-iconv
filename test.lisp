(defvar *t1* (coerce #(90 90 90 90 90 #x87 #x65 #x43 #x21) '(vector (unsigned-byte 8))))
(defvar *t2* (coerce #(90 90 90 90 #xe3 #x82) '(vector (unsigned-byte 8))))
(defvar *t3* (coerce #(97 #xe3 #x81 #x8b) '(vector (unsigned-byte 8))))

#-asdf
(require :asdf)
(asdf:operate 'asdf:load-op :trivial-iconv)
